// Sa se extind˘a aplicatia prezentata în Sectiunea 0.3.1 prin adaugarea unui buton de 
// decrementare si sa se restrictioneze valoarea contorului pe intervalul [0-10]

var count = 0;

function printValue(divId, value) {
	document.getElementById(divId).innerHTML = value;
}

function printRestriction(divId, text) {
	document.getElementById(divId).innerHTML = text;
}

function incr() {
	if (count < 10) {
		count++;
		printValue("message",count);
		printRestriction("restriction", "");
	}
	else {
		printRestriction("restriction", "Maximum is 10.");
	}
}

function decr() {
	if (count > 0) {
		count--;
		printValue("message",count);
		printRestriction("restriction", "");
	}
	else {
		printRestriction("restriction", "Minimum is 0.");
	}
}

printValue("message", 0);
document.getElementById("but_1").addEventListener("click", incr);
document.getElementById("but_2").addEventListener("click", decr);