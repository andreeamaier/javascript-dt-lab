var result;

function getInput(divId){
	return $(divId).val();
}

function printResult(divId,value) {
	$(divId).text(value);
}

function getResult(){
	var a, b, r;
	a = getInput("#in_1");
	b = getInput("#in_2");
	r = parseInt(a) + parseInt(b);
	printResult("#result",r);
}
	
printResult("#result","no operation");
$("#btn_1").on("click", getResult);

