function getFibonacci(n) {
	var fibo = new Array();
	
	if (Number.isInteger(n)) {
		if (n==1) {
			fibo = [1,1];
		}
		else if (n>1 && n<11) {
			fibo = [1,1];
			var x = 2;
			while (x<=n){
				fibo[x] = fibo[x-1]+fibo[x-2];
				x++;
			}
		} 
		else {
			fibo = "not allowed";
		}
	}
	else {
		fibo = "not allowed";
	}
	
	return fibo;
}

console.log(getFibonacci(1));
console.log(getFibonacci(5));
console.log(getFibonacci());
console.log(getFibonacci("ana"));
console.log(getFibonacci(0));
console.log(getFibonacci(15));