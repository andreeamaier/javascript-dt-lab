// Afis, are mesaj la consol˘a din JavaScript (0.25 p)
console.log("Hello JavaScript");

// Modificarea valorii unui câmp text din JavaScript folosind funct,
// ia getElementById accesibila din interfata obiectului document (0.25 p)
document.getElementById("message").innerHTML = "Message from JavaScript";

// Accesarea unei valori dintr-un obiect JSON
var user = {
	"id": 1,
	"name": "Alexandru Popa",
	"address" : {
		"street": "Padin",
		"number": 66,
		"zipcode": "515200"
	},
	"phone": "0741-584-789"
}
console.log(user.name);
console.log(user.address.number);

// Definirea si apelul unei functii JavaScript (0.25 p)
function print(message){
	console.log(message);
}

print("Message printed with a function");

// Utilizarea instructíunii if ca operator ternar (0.25 p)
var pass = "12345";
console.log(pass == "12345" ? "ALLOW" : "DENY");

// Proiectarea unei interfet,e grafice în HTML (0.5 p)
var contor = 0;

function printValue(divId, value) {
	document.getElementById("contor").innerHTML = value;
}

function increment(){
	contor++;
	printValue("contor",contor);
}

document.getElementById("inc").addEventListener("click", increment);

printValue("contor", 0);

// Includerea librariei jQuery în aplicatia JavaScript (0.25 p)

var count = 0;

function printValueJ(divId, value) {
	$(divId).val(value);
}

function incrementJ(){
	count++;
	printValueJ("#contorJ", count);
}

printValueJ("#contorJ", count);
$("#incJ").on("click", incrementJ);




