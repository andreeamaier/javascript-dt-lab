// Sa se modifice logica functiei sum a.i. sa se permita exclusiv introducerea unui nr
// ca argument de intrare. Nu se pot introduce siruri de caractere => se returneaza "not a number".
// sa se scrie 2 teste care verifica introducerea unui sir de caractere si o variabila booleana
// ca argument al functiei sum

function inputSum() {
	var inputNumber = parseInt(document.getElementById("n").value);
	sum(inputNumber);
}

function sum(n){
	if (Number.isInteger(n)) {
		var sum = 0;
		for (var i=0; i<n; i++){
			sum += i;
		}
		return sum;
	}
	else 
		return NaN;
}

document.getElementById("n").addEventListener("input",inputSum);
	